import sys
from PyQt5.QtWidgets import (QMainWindow, QToolTip, QMessageBox,
							QAction, QApplication, QHBoxLayout, QVBoxLayout,
							QPushButton, QWidget, QLineEdit, QLabel,)
from PyQt5.QtGui import QIcon, QFont

from EditorObjects import TextList
from Editor import EditorBox
from IO import ParseItems, ParseTags, ParseRecepies, ParseXML, WriteThings
from Objects import *

#TODO: accessibleName() to text()
#TODO: contextmenu per object?

class MainWindow(QMainWindow):

	def __init__(self, tlist, ilist, rlist):
		super().__init__()
		self.taglist_f = tlist
		if not len(ilist):
			ilist.append(CreateIO())
		self.itemlist_f = ilist
		self.recepielist_f = rlist
		self.taglist = TextList(self.taglist_f, "tag", self)				#lists
		self.itemlist = TextList(self.itemlist_f, "item", self)
		self.recepielist = TextList(self.recepielist_f, "recepie", self)
		self.editor = EditorBox(self.taglist_f, self.itemlist_f, self.recepielist_f, self)
		self.initUI()

	def initUI(self):
		QToolTip.setFont(QFont("SansSerif", 10))

		menubar = self.menuBar()
		filemenu = menubar.addMenu("File")

		loadact = QAction("Import", self)
		loadact.setStatusTip("Load XML files")
		loadact.triggered.connect(self.ReadXmlFiles)

		saveact = QAction("Export", self)
		saveact.setStatusTip("Save XML files")
		saveact.triggered.connect(self.WriteXmlFiles)

		filemenu.addAction(loadact)
		filemenu.addAction(saveact)

		filterbox = QLineEdit()						#filterbox
		filterbox.setMaximumWidth(100)
		filterbox.setPlaceholderText("filter")

		listbox = self.GenerateListBox()					#listbox

		if self.editor.recepies:
			self.editor.DisplayThing(0, "recepie")

		vbox = QVBoxLayout()
		vbox.addWidget(filterbox)
		vbox.addLayout(listbox)
		vbox.addWidget(self.editor)

		mainwidget = QWidget()
		mainwidget.setLayout(vbox)
		self.setCentralWidget(mainwidget)

		#self.statusBar().showMessage("Ready")
		self.setGeometry(300, 300, 640, 480)
		self.setWindowTitle("XML editor")
		self.setWindowIcon(QIcon("Image/Icon.png"))
		self.show()

	def GenerateListBox(self):
		taglabel = QLabel("Tags")
		itemlabel = QLabel("Items")
		recepielabel = QLabel("Recepies")
		taglabel.setFixedHeight(13)
		itemlabel.setFixedHeight(13)
		recepielabel.setFixedHeight(13)

		tagbox = QVBoxLayout()
		tagbox.addWidget(taglabel)
		tagbox.addWidget(self.taglist)

		itembox = QVBoxLayout()
		itembox.addWidget(itemlabel)
		itembox.addWidget(self.itemlist)

		recepiebox = QVBoxLayout()
		recepiebox.addWidget(recepielabel)
		recepiebox.addWidget(self.recepielist)

		listbox = QHBoxLayout()
		listbox.addLayout(tagbox)
		listbox.addStretch(1)
		listbox.addLayout(itembox)
		listbox.addStretch(1)
		listbox.addLayout(recepiebox)

		return listbox

	def closeEvent(self, event):
		reply = QMessageBox.question(self, "Message", "Are you sure you want to exit the program?",
									QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
		if reply == QMessageBox.Yes:
			event.accept()
		else:
			event.ignore()

	def GetEditor(self):
		return self.editor

	def GetTagList(self):
		return self.taglist

	def GetItemList(self):
		return self.itemlist

	def GetRecepieList(self):
		return self.recepielist

	def ExportThing(self, thing, object_type):
		if object_type == "tag":
			for t in self.taglist_f:
				if t["name"] == thing["name"]:
					t = thing
					break
			else:
				self.taglist_f.append(thing)
		elif object_type == "item":
			for t in self.itemlist_f:
				if t["name"] == thing["name"]:
					t = thing
					break
			else:
				self.itemlist_f.append(thing)
		elif object_type == "recepie":
			for t in self.recepielist_f:
				if t["name"] == thing["name"]:
					t = thing
					break
			else:
				self.recepielist_f.append(thing)

	def WriteXmlFiles(self):
		self.taglist_f = self.editor.tags
		self.itemlist_f = self.editor.items
		self.recepielist_f = self.editor.recepies
		WriteThings(self.taglist_f, self.itemlist_f, self.recepielist_f)

	def ReadXmlFiles(self):
		self.taglist_f = ParseTags("xml/Tags.xml")
		self.itemlist_f = ParseItems("xml/Items.xml", self.taglist_f)
		self.recepielist_f = ParseRecepies("xml/Recepies.xml", self.taglist_f, self.itemlist_f)
		self.taglist.SetContent(self.taglist_f)
		self.itemlist.SetContent(self.itemlist_f)
		self.recepielist.SetContent(self.recepielist_f)
		self.editor.SetContent(self.taglist_f, self.itemlist_f, self.recepielist_f)
		if self.editor.recepies:
			self.editor.DisplayThing(0, "recepie")

if __name__ == "__main__":
	# tl = ParseTags("xml/Tags.xml")
	# il = ParseItems("xml/Items.xml", tl)
	# rl = ParseRecepies("xml/Recepies.xml", tl, il)
	tl = []
	il = []
	rl = []
	# tl = ParseTags("xml/Tags - Copy.xml")
	# il = ParseItems("xml/Items - Copy.xml", tl)
	# rl = ParseRecepies("xml/Recepies - Copy.xml", tl, il)

	app = QApplication(sys.argv)
	window = MainWindow(tl, il, rl)
	sys.exit(app.exec_())
