class Tag(dict):
	def __init__(self, name):
		super().__init__()
		self["name"] = name

class Item(dict):
	def __init__(self, name):
		super().__init__()
		self["name"] = name
		self["mesh"] = ""
		self["weight"] = ""
		self["volume"] = ""
		self["value"] = ""
		self["color"] = ""
		self["description"] = ""
		self["tags"] = []

class Recepie(dict):
	def __init__(self, name):
		super().__init__()
		self["name"] = name
		self["components_in"] = []
		self["components_out"] = []
		self["requiredtags"] = []

IO_NAME = "In          Out"
def CreateIO():
	return Item(IO_NAME)
