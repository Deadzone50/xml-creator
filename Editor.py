from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QVBoxLayout, QLabel, QMenu, QPushButton, QLineEdit
from PyQt5.QtCore import Qt, QMimeData, QPoint
from PyQt5.QtGui import QPainter, QPen, QColor, QDrag

from Objects import *
from EditorObjects import (EditorObject, EditorObjectConnectionPoint, EditorEditLine,
							TAGLABELS_H, ITEM_W, ITEM_H)

EDITOR_MIN_W = 300
EDITOR_MIN_H = 300

#TODO: connection tuple?

class EditorBox(QGroupBox):

	def __init__(self, taglist, itemlist, recepielist, parent):
		super().__init__(parent)
		self.SetContent(taglist, itemlist, recepielist)

		self.connections = []

		self.setTitle("Editor")
		self.current_type = "recepie"
		self.current_index = 0
		self.setAcceptDrops(True)
		self.initUI()

	def SetContent(self, taglist, itemlist, recepielist):
		self.tags = taglist
		self.items = itemlist
		self.recepies = recepielist

	def initUI(self):
		self.setMinimumSize(EDITOR_MIN_W, EDITOR_MIN_W)
		self.object_area = QGroupBox()
		self.arealayout = QVBoxLayout()
		self.object_area.setLayout(self.arealayout)
		self.active_tags_w = QHBoxLayout()
		self.active_items_w = []
		self.taglabel = QLabel()
		self.taglabel.setFixedHeight(TAGLABELS_H)

		btn_save = QPushButton("Save")
		btn_save.clicked.connect(self.SaveThing)

		bottomlist = QHBoxLayout()
		bottomlist.addWidget(self.taglabel)
		bottomlist.addLayout(self.active_tags_w)
		bottomlist.addStretch(1)
		bottomlist.addWidget(btn_save)

		vlayout = QVBoxLayout()
		vlayout.addWidget(self.object_area)
		vlayout.addLayout(bottomlist)

		self.setLayout(vlayout)

	def paintEvent(self, event):
		qp = QPainter()
		qp.begin(self)
		self.draw_lines(qp)
		qp.end()
		super().paintEvent(event)

	def draw_lines(self, qp):
		pen = QPen(QColor(160, 30, 30), 3, Qt.SolidLine)
		qp.setPen(pen)
		for con in self.connections:
			con0index, con0num = map(int, con[0].split("#"))
			con1index, con1num = map(int, con[1].split("#"))
			Item1_w = self.GetActiveItemByIndex(con0index)
			Item2_w = self.GetActiveItemByIndex(con1index)
			for Con_w in [child for child in Item1_w.children()
							if isinstance(child, EditorObjectConnectionPoint)]:
				if Con_w.number == con0num:
					point1 = Item1_w.pos()+Con_w.pos()+Con_w.rect().center()
					break
			for Con_w in [child for child in Item2_w.children()
							if isinstance(child, EditorObjectConnectionPoint)]:
				if Con_w.number == con1num:
					point2 = Item2_w.pos()+Con_w.pos()+Con_w.rect().center()
					break
			qp.drawLine(point1, point2)
			del point1
			del point2

	def mousePressEvent(self, event):
		if event.buttons() == Qt.LeftButton:		#dragging objects around
			child = self.childAt(event.pos())
			if isinstance(child, EditorObject):
				if child.object_type == "item" or child.object_type == "IO":
					mimeData = QMimeData()
					mimeData.setText(str(child.index)+":"+child.object_type)

					drag = QDrag(child)
					drag.setMimeData(mimeData)
					drag.setHotSpot(event.pos() - child.rect().topLeft())
					drag.exec_(Qt.MoveAction)
			if isinstance(child, EditorObjectConnectionPoint):
				mimeData = QMimeData()
				mimeData.setText(str(child.parentindex)+"#"+str(child.number)+":link")

				drag = QDrag(child)
				drag.setMimeData(mimeData)
				drag.setHotSpot(event.pos() - child.rect().topLeft())
				drag.exec_(Qt.LinkAction)
		self.update()

	def dragEnterEvent(self, event):
		if event.mimeData().hasFormat("text/plain"):
			event.accept()
		else:
			event.ignore()

	def dropEvent(self, event):
		msg = event.mimeData().text().split(':')
		if msg[1] == "tag":
			widget = self.GetActiveTagByIndex(int(msg[0]))
			if not widget:
				self.AddTagToActive(int(msg[0]))
		elif msg[1] == "item" or msg[1] == "IO":
			widget = self.GetActiveItemByIndex(int(msg[0]))
			position = event.pos() - QPoint(ITEM_W/2, ITEM_H/2)
			if widget:
				widget.move(position)
			else:
				self.AddItemToActive(int(msg[0]), position)
		elif msg[1] == "recepie":
			self.DisplayThing(int(msg[0]), "recepie")
		elif msg[1] == "link":
			child = self.childAt(event.pos())
			if isinstance(child, EditorObjectConnectionPoint):
				connection = [msg[0], str(child.parentindex)+"#"+str(child.number)]
				connection.sort()
				if connection not in self.connections:
					self.connections.append(connection)
		self.update()

	def contextMenuEvent(self, event):
		cm = QMenu(self)
		child = self.childAt(event.pos())
		if isinstance(child, EditorObject):
			edit_act = cm.addAction("Edit")
			amount_act = cm.addAction("Amount")
			delete_act = cm.addAction("Remove")
			if child.object_type == "IO":
				edit_act.setEnabled(False)
				amount_act.setEnabled(False)
				delete_act.setEnabled(False)
			if child.object_type == "tag":
				edit_act.setEnabled(False)
				amount_act.setEnabled(False)
			action = cm.exec_(self.mapToGlobal(event.pos()))
			if action == delete_act:
				if child.object_type == "tag":
					child.deleteLater()
				elif child.object_type == "item":
					self.RemoveItem(child)

	def RemoveTag(self, widget):
		"""Removes tag widget"""
		widget.deleteLater()
		self.update()

	def RemoveItem(self, widget):
		"""Removes item widget and and all connections to it"""
		self.active_items_w.remove(widget)				#TODO: is this needed?
		newc = []
		for con in self.connections:
			if widget.index not in [int(c.split('#')[0]) for c in con]:
				newc.append(con)
		self.connections = newc
		widget.deleteLater()
		self.update()

	def ClearEditor(self):
		"""Clears tags, items and connections"""
		for i in reversed(range(self.active_tags_w.count())):
			self.active_tags_w.itemAt(i).widget().deleteLater()
		for i in self.active_items_w:
			i.deleteLater()
		for i in reversed(range(self.arealayout.count())):
		 	self.arealayout.itemAt(i).widget().setParent(None)
		self.active_items_w.clear()
		self.connections.clear()
		self.update()

	def GetActiveTagByIndex(self, index):
		for i in range(self.active_tags_w.count()):
			w = self.active_tags_w.itemAt(i).widget()
			if w.index == index:
				return w
		return None

	def GetActiveItemByIndex(self, index):
		for t in self.active_items_w:
			if t.index == index:
				return t
		return None

	def RemoveActiveTagByIndex(self, index):
		for i in range(self.active_tags_w.count()):
			w = self.active_tags_w.itemAt(i).widget()
			if w.index == index:
				w.deleteLater()
				self.update()
				break

	def GetTagName(self, index):
		return self.tags[index]["name"]

	def GetItemName(self, index):
		return self.items[index]["name"]

	def GetRecepieName(self, index):
		return self.recepies[index]["name"]

	def AddTagToActive(self, index):
		"""Add tag with {index} to active area"""
		self.active_tags_w.addWidget(EditorObject(index, "tag", 0, self))

	def AddItemToActive(self, index, position):
		"""Add item with {index} to active area at {position}"""
		self.active_items_w.append(EditorObject(index, "item", position, self))

	def AddIOToActive(self, position):
		"""Add the IO item to active area at {position}"""
		self.active_items_w.append(EditorObject(0, "IO", position, self))

	def DisplayThing(self, index, editor_type):
		self.current_type = editor_type
		self.current_index = index
		self.ClearEditor()
		if editor_type == "item":
			self.object_area.setTitle(editor_type.capitalize()+": "+self.GetItemName(index))
			self.taglabel.setText("Item tags:")
			item = self.items[index]
			for tindex in item["tags"]:
				self.AddTagToActive(tindex)
			for p in item.items():
				if p[0] != "tags":
					self.arealayout.addWidget(EditorEditLine(editor_type, p[0], p[1], self))

		elif editor_type == "recepie":
			self.object_area.setTitle(editor_type.capitalize()+": "+self.GetRecepieName(index))
			self.taglabel.setText("Required tags:")
			recepie = self.recepies[index]
			for tindex in recepie["requiredtags"]:
				self.AddTagToActive(tindex)
			i = 0
			for cin in recepie["components_in"]:
				self.AddItemToActive(cin[0], QPoint(10, 50+i*(10+ITEM_H)))
				connection = [str(cin[0])+"#2", "0#1"]
				connection.sort()
				self.connections.append(connection)
				i += 1
			self.AddIOToActive(QPoint((50+3*ITEM_W)/2, 50+((i-1)*(10+ITEM_H))/2))
			i = 0
			for cout in recepie["components_out"]:
				self.AddItemToActive(cout[0], QPoint(40+3*ITEM_W, 50+i*(10+ITEM_H)))
				connection = [str(cout[0])+"#1", "0#2"]
				connection.sort()
				self.connections.append(connection)
				i += 1

	def AddThing(self, name, object_type):
		if object_type == "tag":
			thing = Tag(name)
			self.tags.append(thing)
		elif object_type == "item":
			thing = Item(name)
			self.items.append(thing)
		elif object_type == "recepie":
			thing = Recepie(name)
			self.recepies.append(thing)

	def CheckIfNameExists(self, name, object_type):
		if object_type == "tag":
			for t in self.tags:
				if t["name"] == name:
					return True
			else:
				return False
		elif object_type == "item":
			for t in self.items:
				if t["name"] == name:
					return True
			else:
				return False
		elif object_type == "recepie":
			for t in self.recepies:
				if t["name"] == name:
					return True
			else:
				return False

	def RenameThing(self, index, newname, object_type):
		if object_type == "tag":
			self.tags[index]["name"] = newname
			w = self.GetActiveTagByIndex(index)
			if w:
				w.update()
		elif object_type == "item":
			self.items[index]["name"] = newname
			w = self.GetActiveItemByIndex(index)
			if w:
				w.update()
		elif object_type == "recepie":
			self.recepies[index]["name"] = newname

	def SaveThing(self):
		newlist = []
		if self.current_type == "item":
			current_name = self.GetItemName(self.current_index)
			new_item = Item(current_name)
			for i in range(self.active_tags_w.count()):
				new_item["tags"].append(self.active_tags_w.itemAt(i).widget().index)
			for i in range(self.arealayout.count()):
				w = self.arealayout.itemAt(i).widget()
				if w.object_property == "name":
					self.window().itemlist.ChangeNameIndex(self.current_index, w.tex.text())
				new_item[w.object_property] = w.tex.text()
			for r in self.items:
				if r["name"] == current_name:
					newlist.append(new_item)
				else:
					newlist.append(r)
			self.items = newlist
		elif self.current_type == "recepie":
			current_name = self.GetRecepieName(self.current_index)
			new_recepie = Recepie(current_name)
			for i in range(self.active_tags_w.count()):
				new_recepie["requiredtags"].append(self.active_tags_w.itemAt(i).widget().index)
			for con in self.connections:
				if con[0].startswith("0"):
					if con[0].endswith("#1"):			#in
						new_recepie["components_in"].append([int(con[1].split("#")[0]), 1])
					else:
						new_recepie["components_out"].append([int(con[1].split("#")[0]), 1])
			for r in self.recepies:
				if r["name"] == current_name:
					newlist.append(new_recepie)
				else:
					newlist.append(r)
			self.recepies = newlist

	def DeleteThing(self, index, object_type):
		'''deletes thing at index from relevant list and active area'''
		if object_type == "tag":
			del self.tags[index]
			w = self.GetActiveTagByIndex(index)
			if w is not None:
				self.RemoveTag(w)
			for item in self.items:
				i = 0
				while i < len(item["tags"]):
					if item["tags"][i] == index:
						del item["tags"][i]
						i -= 1
					elif item["tags"][i] > index:
						item["tags"][i] -= 1
					i += 1
			for rec in self.recepies:
				i = 0
				while i < len(rec["requiredtags"]):
					if rec["requiredtags"][i] == index:
						del rec["requiredtags"][i]
						i -= 1
					elif rec["requiredtags"][i] > index:
						rec["requiredtags"][i] -= 1
					i += 1
		elif object_type == "item":
			del self.items[index]
			w = self.GetActiveItemByIndex(index)
			if w is not None:
				self.RemoveItem(w)
			for rec in self.recepies:
				i = 0
				while i < len(rec["components_in"]):
					if rec["components_in"][i] == index:
						del rec["components_in"][i]
						i -= 1
					elif rec["components_in"][i] > index:
						rec["components_in"][i] -= 1
					i += 1
				i = 0
				while i < len(rec["components_out"]):
					if rec["components_out"][i] == index:
						del rec["components_out"][i]
						i -= 1
					elif rec["components_out"][i] > index:
						rec["components_out"][i] -= 1
					i += 1

		elif object_type == "recepie":
			del self.recepies[index]

		self.ClearEditor()
		self.DisplayThing(self.current_index, self.current_type)

	def ImportThing(self, name, index, object_type):
		newlist = []
		found = False
		if object_type == "tag":
			pass
		elif object_type == "item":
			pass
		elif object_type == "recepie":
			pass