from PyQt5.QtWidgets import (QWidget, QLabel, QListWidget, QListWidgetItem,
							QMenu, QFrame, QLineEdit, QItemDelegate, QHBoxLayout)
from PyQt5.QtCore import Qt, QMimeData
from PyQt5.QtGui import QPainter, QPen, QColor, QDrag

TEXTLIST_MAX_H = 170

TAGLABELS_W = 40
TAGLABELS_H = 19

ITEM_W = 120
ITEM_H = 60

CONNECTOR_RADIUS = 7

class TextList(QListWidget):

	def __init__(self, content, object_type, parent):
		super().__init__(parent)
		self.setMaximumHeight(TEXTLIST_MAX_H)
		self.setDragEnabled(True)
		self.listtype = object_type
		# for c in content:
		# 	item = QListWidgetItem(c["name"])
		# 	item.setFlags(item.flags() | Qt.ItemIsEditable)
		# 	self.addItem(item)
		self.SetContent(content)

		self.delegate = QItemDelegate()
		self.setItemDelegate(self.delegate)
		#self.currentTextChanged.connect(self.ChangeName)
		self.delegate.commitData.connect(self.ChangeName)

	def SetContent(self, content):
		self.clear()
		for c in content:
			item = QListWidgetItem(c["name"])
			item.setFlags(item.flags() | Qt.ItemIsEditable)
			self.addItem(item)

	def mouseMoveEvent(self, event):
		if event.buttons() != Qt.LeftButton:
			return
		mimeData = QMimeData()
		row = self.currentRow()
		mimeData.setText(str(row) +':'+self.listtype)
		drag = QDrag(self)
		drag.setMimeData(mimeData)
		drag.exec_(Qt.CopyAction)

	def mouseDoubleClickEvent(self, event):
		if event.buttons() == Qt.LeftButton and self.count():
			row = self.currentRow()
			if self.listtype != "tag":
				self.window().GetEditor().DisplayThing(row, self.listtype)

	def contextMenuEvent(self, event):
		cm = QMenu(self)
		new_act = cm.addAction("New")
		name_act = cm.addAction("Rename")
		edit_act = cm.addAction("Edit")
		delete_act = cm.addAction("Remove")
		import_act = cm.addAction("Re-Import")

		row = self.currentRow()
		if self.listtype == "tag":
			edit_act.setEnabled(False)

		action = cm.exec_(self.mapToGlobal(event.pos()))
		if action == new_act:
			item = QListWidgetItem("New " + self.listtype)
			item.setFlags(item.flags() | Qt.ItemIsEditable)
			self.addItem(item)
			self.window().GetEditor().AddThing("New " + self.listtype, self.listtype)
		elif action == edit_act:
			self.window().GetEditor().DisplayThing(row, self.listtype)
		elif action == delete_act:
			self.window().GetEditor().DeleteThing(row, self.listtype)
			item = self.takeItem(self.currentRow())

		elif action == name_act:
			self.edit(self.currentIndex())
			# newname = "test"
			# name_box = QLineEdit()
			# name_box.setText(newname)
			# name_box.returnPressed.connect(self.ChangeName)

			# self.ChangeName(row, newname)

		elif action == import_act:
			self.window().ImportThing(row, self.listtype)

	def ChangeName(self, editor):
		newname = editor.text()
		row = self.currentRow()
		if not self.window().GetEditor().CheckIfNameExists(newname, self.listtype):
			self.window().GetEditor().RenameThing(row, newname, self.listtype)
			self.currentItem().setText(newname)
		else:
			self.currentItem().setText("Name allready exsists")

	def ChangeNameIndex(self, index, newname):
		i = index
		self.item(i).setText(newname)

class EditorObject(QLabel):

	def __init__(self, index, object_type, position, parent):
		self.index = index
		self.object_type = object_type
		super().__init__(parent)
		self.update()

		self.setFrameStyle(QFrame.Box)
		self.setAlignment(Qt.AlignCenter)
		self.setMargin(2)
		if object_type == "tag":
			self.setFixedHeight(TAGLABELS_H)
		elif object_type == "item":
			self.setAutoFillBackground(True)
			p = self.palette()
			p.setColor(self.backgroundRole(), QColor(20, 120, 20))
			self.setPalette(p)
			self.setFixedSize(ITEM_W, ITEM_H)
			self.move(position)
			self.connection_points = [EditorObjectConnectionPoint(1, self),
									EditorObjectConnectionPoint(2, self),
									EditorObjectConnectionPoint(3, self),
									EditorObjectConnectionPoint(4, self)]
			self.connection_points[0].move(-CONNECTOR_RADIUS, ITEM_H/2-CONNECTOR_RADIUS)
			self.connection_points[1].move(ITEM_W -CONNECTOR_RADIUS, ITEM_H/2 -CONNECTOR_RADIUS)
			self.connection_points[2].move(ITEM_W/2 -CONNECTOR_RADIUS, -CONNECTOR_RADIUS)
			self.connection_points[3].move(ITEM_W/2 -CONNECTOR_RADIUS, ITEM_H -CONNECTOR_RADIUS)
			self.show()
		elif object_type == "IO":
			self.setAutoFillBackground(True)
			p = self.palette()
			p.setColor(self.backgroundRole(), QColor(155, 155, 155))
			self.setPalette(p)
			self.setFixedSize(ITEM_W, ITEM_H)
			self.move(position)
			self.connection_points = [EditorObjectConnectionPoint(1, self),
									EditorObjectConnectionPoint(2, self)]
			self.connection_points[0].move(-CONNECTOR_RADIUS, ITEM_H/2-CONNECTOR_RADIUS)
			self.connection_points[1].move(ITEM_W -CONNECTOR_RADIUS, ITEM_H/2 -CONNECTOR_RADIUS)
			self.show()

	def update(self):
		if self.object_type == "tag":
			text = self.window().GetEditor().GetTagName(self.index)
		elif self.object_type == "item":
			text = self.window().GetEditor().GetItemName(self.index)
		elif self.object_type == "IO":
			text = self.window().GetEditor().GetItemName(self.index)
		else:
			text = "UNKNOWN TYPE"
		self.setText(text)
		super().update()

class EditorObjectConnectionPoint(QWidget):

	def __init__(self, number, parent):
		self.parentindex = parent.index
		self.number = number
		super().__init__(parent)
		self.setFixedSize(2*CONNECTOR_RADIUS+1, 2*CONNECTOR_RADIUS+1)
		self.show()

	def paintEvent(self, event):
		qp = QPainter()
		qp.begin(self)
		self.draw_widget(qp)
		qp.end()

	def draw_widget(self, qp):
		pen = QPen(QColor(20, 20, 20), 1, Qt.SolidLine)
		qp.setPen(pen)
		qp.setBrush(QColor(200, 200, 70))
		qp.drawEllipse(0, 0, CONNECTOR_RADIUS*2, CONNECTOR_RADIUS*2)

class EditorEditLine(QWidget):

	def __init__(self, object_type, object_property, text, parent):
		super().__init__(parent)
		self.object_property = object_property
		self.object_type = object_type
		self.current_text = text

		self.lab = QLabel(object_property)
		self.tex = QLineEdit(text)
		self.lab.setFixedHeight(14)
		self.tex.setFixedHeight(14)
		self.tex.editingFinished.connect(self.ChangeText)
		lay = QHBoxLayout()
		lay.addWidget(self.lab)
		lay.addWidget(self.tex)
		lay.addStretch(1)
		self.setLayout(lay)

	def ChangeText(self):
		text = self.tex.text()
		if self.object_property == "name":
			if self.window().GetEditor().CheckIfNameExists(text, self.object_type):
				text = self.current_text
				#TODO: indicate the name was taken
		self.tex.setText(text)
		