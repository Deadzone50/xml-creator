from Objects import *

def WriteTaglist(filename, taglist):
	f = open(filename, 'w')
	for tag in taglist:
		f.write("<Tag>"+tag+"</Tag\n>")
	f.close()

def AddXmlTag(xml_tree, tag, close):
	if xml_tree[-1] is None:
		xml_tree[-1] = tag
		if close:
			xml_tree.append(None)
	else:
		AddXmlTag(xml_tree[-1], tag, close)

def CloseXmlTag(xml_tree):
	if xml_tree[-1] is None:
		del xml_tree[-1]
		return True
	else:
		if CloseXmlTag(xml_tree[-1]):
			xml_tree.append(None)
		return False

def ParseXML(filename):
	xml_tree = ["root", None]
	f = open(filename, 'r')
	for line in f:
		line = line.replace('>', '<')
		l = line.split('<')
		p_line = [x for x in l if not x.isspace() and x]
		if p_line:
			#for word in p_line:
			if p_line[0][0] == '?':		#version line
				pass
			elif p_line[0][0] == '!':	#comment
				pass
			elif p_line[0][0] == '/':
				CloseXmlTag(xml_tree)
			else:
				if len(p_line) > 1:
					tag = [p_line[0], p_line[1]]
					AddXmlTag(xml_tree, tag, True)
				else:
					tag = [p_line[0], None]
					AddXmlTag(xml_tree, tag, False)
	f.close()
	del xml_tree[-1]	#remove last None
	return xml_tree

def WriteThings(tags, items, recepies):
	f = open("xml/Tags.xml", 'w')
	f.write('<?xml version="1.0" encoding="utf-8"?>\n')
	for t in tags:
		f.write("<tag>"+t["name"]+"</tag>\n")
	f.close()

	f = open("xml/Items.xml", 'w')
	f.write('<?xml version="1.0" encoding="utf-8"?>\n')
	for t in items:
		f.write("<item>\n")
		for p in t.items():
			if p[0] != "tags":
				f.write("\t<"+p[0]+">"+p[1]+"</"+p[0]+">\n")
			else:
				for ti in p[1]:
					f.write("\t<tag>"+tags[ti]["name"]+"</tag>\n")
		f.write("</item>\n")
	f.close()

	f = open("xml/Recepies.xml", 'w')
	f.write('<?xml version="1.0" encoding="utf-8"?>\n')
	for t in recepies:
		f.write("<recepie>\n")
		f.write("\t<name>"+t["name"]+"</name>\n")
		f.write("\t<input>\n")
		for p in t["components_in"]:
			f.write("\t\t<component>\n")
			f.write("\t\t\t<name>"+items[p[0]]["name"]+"</name>\n")
			f.write("\t\t\t<amount>"+str(p[1])+"</amount>\n")
			f.write("\t\t</component>\n")
		f.write("\t</input>\n")

		f.write("\t<output>\n")
		for p in t["components_out"]:
			f.write("\t\t<component>\n")
			f.write("\t\t\t<name>"+items[p[0]]["name"]+"</name>\n")
			f.write("\t\t\t<amount>"+str(p[1])+"</amount>\n")
			f.write("\t\t</component>\n")
		f.write("\t</output>\n")
		for rt in t["requiredtags"]:
			f.write("\t<requirement>"+tags[rt]["name"]+"</requirement>\n")
		f.write("</recepie>\n")
	f.close()

def ParseTags(filename):
	xml_tree = ParseXML(filename)
	taglist = []
	for xml_e in xml_tree:
		if xml_e[0] == "tag":
			taglist.append(Tag(xml_e[1]))
	return taglist

def ParseItems(filename, taglist):
	itemlist = []
	xml_tree = ParseXML(filename)
	for xml_e in xml_tree:
		if xml_e[0] != "item":
			continue
		item = Item("")
		for p in xml_e[1:]:
			if p[0] == "tag":
				i = 0
				for t in taglist:
					if t["name"] == p[1]:
						item["tags"].append(i)
						break
					i += 1
				else:
					taglist.append(Tag(p[1]))
					item["tags"].append(i)
			else:
				item[p[0]] = p[1]
		itemlist.append(item)
	if itemlist[0]["name"] != IO_NAME:
		itemlist.insert(0, CreateIO())
	return itemlist

def ParseComponents(complist, itemlist):
	result = []
	for comp in complist:
		name = ""
		amount = 0
		if comp[1][0] == "name":
			name = comp[1][1]
		if comp[2][0] == "amount":
			amount = int(comp[2][1])
		i = 0
		for t in itemlist:
			if t["name"] == name:
				result.append([i, amount])
				break
			i += 1
		else:
			raise NameError(name+" not in itemlist")
	return result

def ParseRecepies(filename, taglist, itemlist):
	recepielist = []
	xml_tree = ParseXML(filename)
	for xml_e in xml_tree:
		if xml_e[0] != "recepie":
			continue
		rec = Recepie("")
		for p in xml_e[1:]:
			if p[0] == "requirement":
				i = 0
				for t in taglist:
					if t["name"] == p[1]:
						rec["requiredtags"].append(i)
						break
					i += 1
				else:
					taglist.append(Tag(p[1]))
					rec["requiredtags"].append(i)
			elif p[0] == "input":
 				rec["components_in"] = (ParseComponents(p[1:], itemlist))
			elif p[0] == "output":
				rec["components_out"] = (ParseComponents(p[1:], itemlist))
			elif p[0] == "component":
				pass
			else:
				rec[p[0]] = p[1]
		recepielist.append(rec)
	return recepielist
